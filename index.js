// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import Middleware
const { printTimedMiddleware } = require("./app/middleware/requestTimeMiddleware.js")
const { printMethodMiddleware } = require("./app/middleware/requestMethodMiddleware.js");

// Import Model
const userModel = require("./app/model/userModel.js");
const diceHistoryModel = require("./app/model/diceHistoryModel.js");
const prizeModel = require("./app/model/prizeModel.js");
const voucherModel = require("./app/model/voucherModel.js");
const prizeHistoryModel = require("./app/model/prizeHistoryModel.js");
const voucherHistoryModel = require("./app/model/voucherHistoryModel.js");

// Import Router
const viewRouter = require("./app/routes/viewRouter.js");
const userRouter = require("./app/routes/userRouter.js");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter.js");
const prizeRouter = require("./app/routes/prizeRouter.js");
const voucherRouter = require("./app/routes/voucherRouter.js");
const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter.js");
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter.js");
const diceRouter = require("./app/routes/diceRouter.js");

// Import mongooseJS
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Cấu hình để app đọc được body request dạng json
app.use(express.json());
// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

// Khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_LuckyDiceNoteJs", (err) => {
    if(err) {
        throw err;
    };
    console.log("Connect MongoDB successfully!");
})

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/random-number", printTimedMiddleware, printMethodMiddleware, (request, response) => {
    let number = Math.floor(Math.random() * 6) + 1;

    response.status(200).json({
        message: `Random 1 số tự nhiên có giá trị ngẫu nhiên từ 1 đến 6: ${number}`
    })
    console.log(`Random 1 số tự nhiên có giá trị ngẫu nhiên từ 1 đến 6: ${number}`)
})

app.use("/", viewRouter);
app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", prizeHistoryRouter);
app.use("/", voucherHistoryRouter);
app.use("/", diceRouter);


// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})

