/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "/devcamp-lucky-dice"
const gUTF8_TEXT_APPLICATION_HEADER = "application/json;charsetUTF-8";
const gEND_ROW_COL = -1;
const gCOL_ID_VOUCHER = 0;
const gCOL_MA_VOUCHER = 1;
const gCOL_PHAN_TRAM_GIAM_GIA = 2;
const gCOL_GHI_CHU = 3;
const gCOL_NGAY_TAO = 4;
const gCOL_NGAY_CAP_NHAT = 5;
const gCOL_STT = 0;
const gCOL_DICE = 1;
const gCOL_PRIZE = 1;

var gSelectButtonValue = {
    value: ""
}
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    "use strict"
    onPageLoading()

    $("#btn-dice").on("click", function () {
        onBtnDiceClick()
    });
    $("#btn-dice-history").on("click", function () {
        onBtnDiceHistoryClick()
    });
    $("#btn-voucher-history").on("click", function () {
        onBtnVoucherHistoryClick()
    });
    $("#btn-present-history").on("click", function () {
        onBtnPresentHistoryClick()
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Function get new dice handle
function onBtnGetNewDiceClick() {
    "use strict";
    //base urlbaseUrk
    const vBaseUrl = "http://42.115.221.44:8080/devcamp-lucky-dice/";
    const vUtf8TextApplicationHeader = "application/json;charset=UTF-8";
    // data to be sent by post method
    var vDataObject = {
        firstname: "do",
        lastname: "nga",
        username: "ngado20"
    }



}

// hàm xử lý sự kiện loading
function onPageLoading() {
    "use strict";
}
// hàm xử lý sự kiện ấn nút Ném
function onBtnDiceClick() {
    "use strict";
    // tạo đối tượng 
    var vUserObj = {
        username: "",
        firstname: "",
        lastname: ""
    }
    // bước 1: đọc dữ liệu
    readDataUserObj(vUserObj)
    // bước 2 : validate
    var checkData = validateData(vUserObj)
    if (checkData) {
        changeColorButtonDice()
        // bước 3: gọi Api sever
        callApiSeverGetNewDice(vUserObj)
        // bước 4: xử lý response trả về từ sever 
    }
}
// hàm thực hiện khi ấn nút Dice History
function onBtnDiceHistoryClick() {
    "use strict";
    console.log("Press Button Dice History");
    // tạo đối tượng 
    var vUserObj = {
        username: "",
        firstname: "",
        lastname: ""
    }
    // bước 1: đọc dữ liệu
    readDataUserObj(vUserObj)
    // bước 2 : validate
    var checkData = validateData(vUserObj)
    if (checkData) {
        gSelectButtonValue.value = "select-dice-history";
        changeColorSelectButton(gSelectButtonValue.value);
        // bước 3: gọi Api sever
        callApiSeverGetDiceHistory(vUserObj);
        // bước 4: xử lý response trả về từ sever 
    }
}
// hàm thực hiện khia ấn nút Voucher History
function onBtnVoucherHistoryClick() {
    "use strict";
    console.log("Press Button Voucher History");
    // tạo đối tượng 
    var vUserObj = {
        username: "",
        firstname: "",
        lastname: ""
    }
    // bước 1: đọc dữ liệu
    readDataUserObj(vUserObj)
    // bước 2 : validate
    var checkData = validateData(vUserObj)
    if (checkData) {
        gSelectButtonValue.value = "select-voucher-history";
        changeColorSelectButton(gSelectButtonValue.value);
        // bước 3: gọi Api sever
        callApiSeverGetVoucherHistory(vUserObj);
        // bước 4: xử lý response trả về từ sever 
    }
}
// hàm thực hiện khia ấn nút Present History
function onBtnPresentHistoryClick() {
    "use strict";
    console.log("Press Button Voucher History");
    // tạo đối tượng 
    var vUserObj = {
        username: "",
        firstname: "",
        lastname: ""
    }
    // bước 1: đọc dữ liệu
    readDataUserObj(vUserObj);
    // bước 2 : validate
    var vValidateData = validateData(vUserObj);
    if (vValidateData) {
        gSelectButtonValue.value = "select-present-history";
        changeColorSelectButton(gSelectButtonValue.value);
        // bước 3: gọi Api sever
        callApiSeverGetPresentHistory(vUserObj);
        // bước 4: xử lý response trả về từ sever
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm thay đổi button Dice khi ấn
function changeColorButtonDice() {
    "use strict";
    $("#btn-dice")
        .removeClass("btn-primary")
        .addClass("btn-success")
}
// hàm thay đồi màu khi chọn Table History
function changeColorSelectButton(paramButton) {
    "use strict";
    var vBtnDiceHistory = document.getElementById("btn-dice-history");
    var vBtnVoucherHistory = document.getElementById("btn-voucher-history");
    var vBtnPresentHistory = document.getElementById("btn-present-history");

    if (paramButton === "select-dice-history") {
        vBtnDiceHistory.className = "btn btn-success button-width w-25";
        vBtnVoucherHistory.className = "btn btn-primary button-width w-25";
        vBtnPresentHistory.className = "btn btn-primary button-width w-25";
    }
    else if (paramButton === "select-voucher-history") {
        vBtnDiceHistory.className = "btn btn-primary button-width w-25";
        vBtnVoucherHistory.className = "btn btn-success button-width w-25";
        vBtnPresentHistory.className = "btn btn-primary button-width w-25";
    }
    else if (paramButton === "select-present-history") {
        vBtnDiceHistory.className = "btn btn-primary button-width w-25";
        vBtnVoucherHistory.className = "btn btn-primary button-width w-25";
        vBtnPresentHistory.className = "btn btn-success button-width w-25";
    }
}
// hàm đọc dữ liệu bước 1
function readDataUserObj(paramUserObj) {
    "use strict";
    paramUserObj.username = $("#inp-user-name").val().trim();
    paramUserObj.firstname = $("#inp-first-name").val().trim();
    paramUserObj.lastname = $("#inp-last-name").val().trim();
}
// hàm kiểm tra dữ liệu
function validateData(paramUserObj) {
    "use strict";
    if (paramUserObj.username === "") {
        $("#inp-user-name").addClass("is-invalid");
        $('#valid-name')
            .addClass("invalid-feedback text-left")
            .html("Name không được bỏ trống")
        alert("Chưa nhập Name !!!")
        return false;
    }
    else {
        $("#inp-user-name")
            .removeClass("is-invalid")
            .addClass("is-valid");
    }
    if (paramUserObj.firstname === "") {
        $("#inp-first-name").addClass("is-invalid");
        $('#valid-first-name')
            .addClass("invalid-feedback text-left")
            .html("First Name không được bỏ trống")
        alert("Chưa nhập First Name !!!")
        return false;
    }
    else {
        $("#inp-first-name")
            .removeClass("is-invalid")
            .addClass("is-valid");
    }
    if (paramUserObj.lastname === "") {
        $("#inp-last-name").addClass("is-invalid");
        $('#valid-last-name')
            .addClass("invalid-feedback text-left")
            .html("Last Name không được bỏ trống")
        alert("Chưa nhập Last Name !!!")
        return false;
    }
    else {
        $("#inp-last-name")
            .removeClass("is-invalid")
            .addClass("is-valid");
    }
    return true;
}
// hàm gọi api từ sever get new dice
function callApiSeverGetNewDice(paramUserObj) {
    "use strict";
    $.ajax({
        url: gBASE_URL + "/dice",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(paramUserObj),
        success: function (responseObj) {
            displayResults(responseObj);
        },
        error: function (error) {
            alert(error.responseText);
        }
    })
}
// hàm gọi api từ sever get dice history
function callApiSeverGetDiceHistory(paramUserObj) {
    "use strict";
    $.ajax({
        url: gBASE_URL + "/dice-history?username=" + paramUserObj.username,
        type: 'GET',
        dataType: 'json',
        success: function (responseObj) {
            showDataOnTableOfDiceHistory(responseObj)
        },
        error: function (error) {
            alert(error.responseText);
        }
    })
}
// hàm gọi api từ sever get voucher history
function callApiSeverGetVoucherHistory(paramUserObj) {
    "use strict";
    $.ajax({
        url: gBASE_URL + "/voucher-history?username=" + paramUserObj.username,
        type: 'GET',
        dataType: 'json',
        success: function (responseObj) {
            showDataOnTableOfVoucherHistory(responseObj);
        },
        error: function (error) {
            alert(error.responseText);
        }
    })
}
// hàm gọi api từ sever get present history
function callApiSeverGetPresentHistory(paramUserObj) {
    "use strict";
    $.ajax({
        url: gBASE_URL + "/prize-history?username=" + paramUserObj.username,
        type: 'GET',
        dataType: 'json',
        success: function (responseObj) {
            showDataOnTableOfPresentHistory(responseObj);
        },
        error: function (error) {
            alert(error.responseText);
        }
    })
}
// hàm xử lý response trả về từ sever 
function displayResults(paramResponseObj) {
    "use strict";
    // chuyển Json thành Obj
    var vResponse = JSON.stringify(paramResponseObj)
    console.log(vResponse);
    // get result dice
    var vDiceReady = paramResponseObj.dice;
    console.log("dice result: " + vDiceReady);
    // gọi hàm hiện thị Dice
    changeDice(vDiceReady)
    //gọi hàm hiện thị lời nhắn
    changNotification(vDiceReady);
    // gọi hàm hiển thị voucher
    changeVoucher(paramResponseObj);
    // gọi hàm hiển thị prize
    changePrize(paramResponseObj);
}
// hàm hiển thị dice
function changeDice(paramDice) {
    "use strict";
    var vImgDice = $("#img-dice");
    switch (paramDice) {
        case 1:
            vImgDice.attr("src", "LuckyDiceImages/1.png")
            break;
        case 2:
            vImgDice.attr("src", "LuckyDiceImages/2.png")
            break;
        case 3:
            vImgDice.attr("src", "LuckyDiceImages/3.png")
            break;
        case 4:
            vImgDice.attr("src", "LuckyDiceImages/4.png")
            break;
        case 5:
            vImgDice.attr("src", "LuckyDiceImages/5.png")
            break;
        default:
            vImgDice.attr("src", "LuckyDiceImages/6.png")
    }
}
// hàm hiển thị lời nhắn
function changNotification(paramDice) {
    "use strict";
    var vNotification = $("#p-notification-dice");
    if (paramDice < 4) {
        vNotification.html("Chúc mừng bạn may mắn lần sau !!!")
    }
    else {
        vNotification.html("Chúc mừng bạn hãy chơi tiếp lần nữa !!!")
    }
}
// hàm hiển thị voucher
function changeVoucher(paramResponseObj) {
    "use strict";
    if (paramResponseObj.voucher != null) {
        $("#p-voucher-id").html("Mã Voucher: " + paramResponseObj.voucher.code);
        $("#p-voucher-percent").html("Phần trăm giảm giá: " + paramResponseObj.voucher.discount + "%");
    } else {
        $("#p-voucher-id").html("");
        $("#p-voucher-percent").html("Không có mã giảm giá");
    }

    if (paramResponseObj.prize != null) {
        $("#p-present").html("Phần thưởng bonus: " + paramResponseObj.prize)
    } else {
        $("#p-present").html("Không có phần thưởng bonus")
    }
}
// hàm hiển thị prize
function changePrize(paramResponsePrize) {
    "use strict";
    var vImgPresent = $("#img-present");
    var vTenPhanThuong = paramResponsePrize.prize

    switch (vTenPhanThuong) {
        case "Mũ":
            vImgPresent.attr("src", "LuckyDiceImages/hat.jpg")
            break;
        case "Áo":
            vImgPresent.attr("src", "LuckyDiceImages/t-shirt.jpg")
            break;
        case "Ô tô":
            vImgPresent.attr("src", "LuckyDiceImages/car.jpg")
            break;
        case "Xe máy":
            vImgPresent.attr("src", "LuckyDiceImages/motobike.jpg")
            break;
        case "Xe đạp":
            vImgPresent.attr("src", "LuckyDiceImages/bike.jpg")
            break;
        default:
            vImgPresent.attr("src", "LuckyDiceImages/no-present.jpg")
    }
}
// hàm xử lý hiện thị dữ liệu vào bảng Dice History
function showDataOnTableOfDiceHistory(paramResponseObj) {
    "use strict";
    var vDiceHistory = JSON.stringify(paramResponseObj)
    console.log(vDiceHistory);

    var vHistoryTable = $("#history-table");
    $("#history-table").empty();
    var bNewRow = $("<tr>").appendTo(vHistoryTable)
    $("<th>", { html: "Lượt" }).appendTo(bNewRow);
    $("<th>", { html: "Dice" }).appendTo(bNewRow);
    for (var bIndex = 0; bIndex < (paramResponseObj.dices).length; bIndex++) {
        var vHistoryTable = $("#history-table");
        var bNewRow = $("<tr>").appendTo(vHistoryTable)

        $("<td>", { html: bIndex + 1 }).appendTo(bNewRow);
        $("<td>", { html: paramResponseObj.dices[bIndex] }).appendTo(bNewRow);

    }
}
// hàm xử lý hiện thị dữ liệu vào bảng Voucher History
function showDataOnTableOfVoucherHistory(paramResponseObj) {
    "use strict";
    var vVoucherHistory = JSON.stringify(paramResponseObj)
    console.log(vVoucherHistory);
    var vHistoryTable = $("#history-table");
    $("#history-table").empty();
    var bNewRow = $("<tr>").appendTo(vHistoryTable).addClass("text-center")
    $("<th>", { html: "ID" }).appendTo(bNewRow);
    $("<th>", { html: "Mã Voucher" }).appendTo(bNewRow);
    $("<th>", { html: "Phần Trăm Giảm Gía" }).appendTo(bNewRow);
    $("<th>", { html: "Ghi Chú" }).appendTo(bNewRow);
    $("<th>", { html: "Ngày Tạo" }).appendTo(bNewRow);
    $("<th>", { html: "Ngày Cập Nhật" }).appendTo(bNewRow);
    for (var bIndex = 0; bIndex < (paramResponseObj.vouchers).length; bIndex++) {
        var vHistoryTable = $("#history-table");
        var bNewRow = $("<tr>").appendTo(vHistoryTable).addClass("text-center")

        $("<td>", { html: paramResponseObj.vouchers[bIndex]._id }).appendTo(bNewRow);
        $("<td>", { html: paramResponseObj.vouchers[bIndex].code }).appendTo(bNewRow);
        $("<td>", { html: paramResponseObj.vouchers[bIndex].discount + "%" }).appendTo(bNewRow);
        $("<td>", { html: ghiChu(paramResponseObj.vouchers[bIndex].note) }).appendTo(bNewRow);
        $("<td>", { html: paramResponseObj.vouchers[bIndex].ngayTao }).appendTo(bNewRow);
        $("<td>", { html: paramResponseObj.vouchers[bIndex].ngayCapNhat }).appendTo(bNewRow);
    }
    function ghiChu(paramGhiChu) {
        if (!paramGhiChu || paramGhiChu === null) {
            return "Không có ghi chú"
        } else {
            return paramGhiChu;
        }
    }
}
// hàm xử lý hiện thị dữ liệu vào bảng Present History
function showDataOnTableOfPresentHistory(paramResponseObj) {
    "use strict";
    var vPrizeHistory = JSON.stringify(paramResponseObj)
    console.log(vPrizeHistory);
    var vHistoryTable = $("#history-table");
    $("#history-table").empty();
    var bNewRow = $("<tr>").appendTo(vHistoryTable).addClass("text-center")
    $("<th>", { html: "Lượt" }).appendTo(bNewRow);
    $("<th>", { html: "Prize" }).appendTo(bNewRow);
    $("<th>", { html: "Image Prize" }).appendTo(bNewRow);

    for (var bIndex = 0; bIndex < (paramResponseObj.prizes).length; bIndex++) {
        var vHistoryTable = $("#history-table");
        var bNewRow = $("<tr>").appendTo(vHistoryTable).addClass("text-center")

        $("<td>", { html: bIndex + 1 }).appendTo(bNewRow);
        $("<td>", { html: paramResponseObj.prizes[bIndex] }).appendTo(bNewRow);
        $("<img>").attr("src", prizeImage(paramResponseObj.prizes[bIndex]))
            .appendTo($("<td>").appendTo(bNewRow)).addClass("picture-prize")
    }
    function prizeImage(paramPrize) {
        switch (paramPrize) {
            case "Mũ":
                return "./LuckyDiceImages/hat.jpg" ;
                break;
            case "Áo":
                return "./LuckyDiceImages/t-shirt.jpg";
                break;
            case "Ô tô":
                return "./LuckyDiceImages/car.jpg";
                break;
            case "Xe máy":
                return "./LuckyDiceImages/motobike.jpg";
                break;
            case "Xe đạp":
                return "./LuckyDiceImages/bike.jpg";
                break;
            default:
                return "./LuckyDiceImages/no-present.jpg";
        }
    }
}

