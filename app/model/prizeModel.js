// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const prizeSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("prize", prizeSchema);