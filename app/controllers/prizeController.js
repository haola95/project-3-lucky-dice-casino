// Import prize model vào controller
const prizeModel = require("../model/prizeModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createPrize = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!bodyRequest.name) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Name is required"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    let createPrize = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
    }

    prizeModel.create(createPrize, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created Prize Success ",
                data: data
            })
        }

    })
}

const getAllPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Prize",
                data: data
            })
        }
    })
}

const getPrizerById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let prizeId = request.params.prizeId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Prize By Id: " + prizeId,
                data: data
            })
        }
    })


}

const updatePrizeById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let prizeId = request.params.prizeId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let prizeUpdate = {
        name: bodyRequest.name,
    }

    prizeModel.findByIdAndUpdate(prizeId, prizeUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Prize success",
                data: data
            })
        }
    })
}

const deletePrizeById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let prizeId = request.params.prizeId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Delete Prize: " + prizeId + " Success "
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createPrize: createPrize,
    getAllPrize: getAllPrize,
    getPrizerById: getPrizerById,
    updatePrizeById: updatePrizeById,
    deletePrizeById: deletePrizeById
}