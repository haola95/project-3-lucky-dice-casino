// Import prize History model vào controller
const prizeHistoryModel = require("../model/prizeHistoryModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createPrizeHistory = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    // B2: Validate dữ liệu
    if (!bodyRequest.user) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User is required"
        })
    }

    if (!bodyRequest.prize) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize is required"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let createPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        prize: bodyRequest.prize
    }
    prizeHistoryModel.create(createPrizeHistory, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created Prize History Success ",
                data: data
            })
        }
    })
}

const getAllPrizeHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let user = request.query.user;

    let condition = {};

    if(user) {
        condition.user = user;
    }
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    prizeHistoryModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Prize",
                data: data
            })
        }
    })
}

const getPrizeHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let historyId = request.params.historyId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    prizeHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Prize History By Id: " + historyId,
                data: data
            })
        }
    })


}

const updatePrizeHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let historyId = request.params.historyId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let prizeHistoryUpdate = {
        user: bodyRequest.user,
        prize: bodyRequest.prize
    }

    prizeHistoryModel.findByIdAndUpdate(historyId, prizeHistoryUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Prize History success",
                data: data
            })
        }
    })
}

const deletePrizeHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let historyId = request.params.historyId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Prize History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    prizeHistoryModel.findByIdAndDelete(historyId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Delete Prize History ID: " + historyId + " Success"
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createPrizeHistory: createPrizeHistory,
    getAllPrizeHistory: getAllPrizeHistory,
    getPrizeHistoryById: getPrizeHistoryById,
    updatePrizeHistoryById: updatePrizeHistoryById,
    deletePrizeHistoryById: deletePrizeHistoryById
}