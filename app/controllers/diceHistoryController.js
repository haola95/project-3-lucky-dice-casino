// Import dice history model vào controller
const diceHistoryModel = require("../model/diceHistoryModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createDiceHistory = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!bodyRequest.user) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User is required"
        })
    }

    if (!bodyRequest.dice) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.dice) && bodyRequest.dice > 0 && bodyRequest.dice < 7)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice is number and max 6 min 1"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    let createDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        dice: Math.floor(Math.random() * 6) + 1
    }

    diceHistoryModel.create(createDiceHistory, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created Dice History Success ",
                data: data
            })
        }

    })
}

const getAllDiceHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let user = request.query.user;

    let condition = {};

    if(user) {
        condition.user = user;
    }
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    diceHistoryModel.find(condition,(error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get All Dice History",
                data: data
            })
        }
    })
}

const getDiceHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(diceHistoryId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    diceHistoryModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Dice History By Id: " + diceHistoryId,
                data: data
            })
        }
    })


}

const updateDiceHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(diceHistoryId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let diceHistoryUpdate = {
        user: bodyRequest.user,
        dice: bodyRequest.dice
        
    }

    diceHistoryModel.findByIdAndUpdate(diceHistoryId, diceHistoryUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Dice History success",
                data: data
            })
        }
    })
}

const deleteDiceHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(diceHistoryId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Dice History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Delete Dice History: " + diceHistoryId + " Success"
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createDiceHistory: createDiceHistory,
    getAllDiceHistory: getAllDiceHistory,
    getDiceHistoryById: getDiceHistoryById,
    updateDiceHistoryById: updateDiceHistoryById,
    deleteDiceHistoryById: deleteDiceHistoryById
}