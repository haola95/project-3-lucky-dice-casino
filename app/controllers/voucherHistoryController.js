// Import voucher History model vào controller
const voucherHistoryModel = require("../model/voucherHistoryModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createVoucherHistory = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    // B2: Validate dữ liệu
    if (!bodyRequest.user) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User is required"
        })
    }

    if (!bodyRequest.voucher) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher is required"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let createVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        voucher: bodyRequest.voucher
    }
    voucherHistoryModel.create(createVoucherHistory, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created Voucher History Success ",
                data: data
            })
        }
    })
}

const getAllVoucherHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let user = request.query.user;

    let condition = {};

    if(user) {
        condition.user = user;
    }
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    voucherHistoryModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            if(user) {
                response.status(200).json({
                    status: "Success: Get All Voucher Hisotry Of UserId " + user,
                    data: data
                })
            } else {
                response.status(200).json({
                    status: "Success: Get All Voucher Hisotry",
                    data: data
                })
            }
            
        }
    })
}

const getVoucherHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let historyId = request.params.historyId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    voucherHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Voucher History By Id: " + historyId,
                data: data
            })
        }
    })


}

const updateVoucherHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let historyId = request.params.historyId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let prizeHistoryUpdate = {
        user: bodyRequest.user,
        voucher: bodyRequest.voucher
    }

    voucherHistoryModel.findByIdAndUpdate(historyId, prizeHistoryUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Voucher History success",
                data: data
            })
        }
    })
}

const deleteVoucherHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let historyId = request.params.historyId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher History ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    voucherHistoryModel.findByIdAndDelete(historyId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Delete Voucher History ID: " + historyId + " Success"
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createVoucherHistory: createVoucherHistory,
    getAllVoucherHistory: getAllVoucherHistory,
    getVoucherHistoryById: getVoucherHistoryById,
    updateVoucherHistoryById: updateVoucherHistoryById,
    deleteVoucherHistoryById: deleteVoucherHistoryById
}