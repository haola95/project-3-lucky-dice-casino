// Import voucher model vào controller
const voucherModel = require("../model/voucherModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createVoucher = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest)
    // B2: Validate dữ liệu
    if (!bodyRequest.code) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Code is required"
        })
    }

    if (!bodyRequest.discount) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Discount is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.discount))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Discount is number"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let createVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: bodyRequest.code,
        discount: bodyRequest.discount,
        note: bodyRequest.note
    }

    voucherModel.create(createVoucher, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created Voucher Success ",
                data: data
            })
        }

    })
}

const getAllVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Voucher",
                data: data
            })
        }
    })
}

const getVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get Voucher By Id: " + voucherId,
                data: data
            })
        }
    })


}

const updateVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let voucherUpdate = {
        code: bodyRequest.code,
        discount: bodyRequest.discount,
        note: bodyRequest.note,
    }

    voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Update Voucher success",
                data: data
            })
        }
    })
}

const deleteVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Delete Voucher Id : " + voucherId + " Success "
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createVoucher: createVoucher,
    getAllVoucher: getAllVoucher,
    getVoucherById: getVoucherById,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById
}