// Import bo thu vien express
const express = require('express');

// Import Path
const path = require("path")

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

const router = express.Router();

router.use(express.static(__dirname + "../../../views"))

router.get("/", printTimedMiddleware, printMethodMiddleware, (request, response) => {
    console.log(__dirname)
    response.sendFile(path.join(__dirname+'../../../views/Ma nguon Lucky Dice Casino.html'));
});

// Export dữ liệu 1 module
module.exports = router;