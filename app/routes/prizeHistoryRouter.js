// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const {createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById , deletePrizeHistoryById} = require("../controllers/prizeHistoryController.js")

const router = express.Router();

router.get("/prize-histories", printTimedMiddleware, printMethodMiddleware, getAllPrizeHistory);

router.post("/prize-histories", printTimedMiddleware, printMethodMiddleware, createPrizeHistory);

router.get("/prize-histories/:historyId", printTimedMiddleware, printMethodMiddleware, getPrizeHistoryById);

router.put("/prize-histories/:historyId", printTimedMiddleware, printMethodMiddleware, updatePrizeHistoryById);

router.delete("/prize-histories/:historyId", printTimedMiddleware, printMethodMiddleware, deletePrizeHistoryById);

// Export dữ liệu 1 module
module.exports = router;

