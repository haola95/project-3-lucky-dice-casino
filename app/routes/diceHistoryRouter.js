// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const {createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById } = require("../controllers/diceHistoryController.js")

const router = express.Router();

router.get("/dice-histories", printTimedMiddleware, printMethodMiddleware, getAllDiceHistory);

router.post("/dice-histories", printTimedMiddleware, printMethodMiddleware, createDiceHistory);

router.get("/dice-histories/:diceHistoryId", printTimedMiddleware, printMethodMiddleware, getDiceHistoryById);

router.put("/dice-histories/:diceHistoryId", printTimedMiddleware, printMethodMiddleware, updateDiceHistoryById);

router.delete("/dice-histories/:diceHistoryId", printTimedMiddleware, printMethodMiddleware, deleteDiceHistoryById);


// Export dữ liệu 1 module
module.exports = router;