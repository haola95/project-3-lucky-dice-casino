// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const {createPrize, getAllPrize, getPrizerById, updatePrizeById, deletePrizeById } = require("../controllers/prizeController.js")

const router = express.Router();

router.get("/prizes", printTimedMiddleware, printMethodMiddleware, getAllPrize);

router.post("/prizes", printTimedMiddleware, printMethodMiddleware, createPrize);

router.get("/prizes/:prizeId", printTimedMiddleware, printMethodMiddleware, getPrizerById);

router.put("/prizes/:prizeId", printTimedMiddleware, printMethodMiddleware, updatePrizeById);

router.delete("/prizes/:prizeId", printTimedMiddleware, printMethodMiddleware, deletePrizeById);

// Export dữ liệu 1 module
module.exports = router;

