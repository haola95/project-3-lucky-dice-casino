// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const {createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/userController.js")

const router = express.Router();

router.get("/users", printTimedMiddleware, printMethodMiddleware, getAllUser);

router.post("/users", printTimedMiddleware, printMethodMiddleware, createUser);

router.get("/users/:userId", printTimedMiddleware, printMethodMiddleware, getUserById);

router.put("/users/:userId", printTimedMiddleware, printMethodMiddleware, updateUserById);

router.delete("/users/:userId", printTimedMiddleware, printMethodMiddleware, deleteUserById);

// Export dữ liệu 1 module
module.exports = router;

