// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const {createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require("../controllers/voucherController.js")

const router = express.Router();

router.get("/vouchers", printTimedMiddleware, printMethodMiddleware, getAllVoucher);

router.post("/vouchers", printTimedMiddleware, printMethodMiddleware, createVoucher);

router.get("/vouchers/:voucherId", printTimedMiddleware, printMethodMiddleware, getVoucherById);

router.put("/vouchers/:voucherId", printTimedMiddleware, printMethodMiddleware, updateVoucherById);

router.delete("/vouchers/:voucherId", printTimedMiddleware, printMethodMiddleware, deleteVoucherById);

// Export dữ liệu 1 module
module.exports = router;

