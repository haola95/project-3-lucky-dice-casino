// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const { diceHandler, getDiceHistory, getPrizeHistory, getVoucherHistory} = require("../controllers/diceController.js")

const router = express.Router();

router.post("/devcamp-lucky-dice/dice", printTimedMiddleware, printMethodMiddleware, diceHandler);

//devcamp-lucky-dice/dice-history?username=:username
router.get("/devcamp-lucky-dice/dice-history", printTimedMiddleware, printMethodMiddleware, getDiceHistory);

//devcamp-lucky-dice/prize-history?username=:username
router.get("/devcamp-lucky-dice/prize-history", printTimedMiddleware, printMethodMiddleware, getPrizeHistory);

//devcamp-lucky-dice/prize-history?username=:username
router.get("/devcamp-lucky-dice/voucher-history", printTimedMiddleware, printMethodMiddleware, getVoucherHistory);

// Export dữ liệu 1 module
module.exports = router;

