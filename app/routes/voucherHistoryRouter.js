// Import bo thu vien express
const express = require('express');

// Import Middleware
const { printTimedMiddleware } = require("../middleware/requestTimeMiddleware.js");
const { printMethodMiddleware } = require("../middleware/requestMethodMiddleware.js");

// Import Controller
const {createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById , deleteVoucherHistoryById} = require("../controllers/voucherHistoryController.js")

const router = express.Router();

router.get("/voucher-histories", printTimedMiddleware, printMethodMiddleware, getAllVoucherHistory);

router.post("/voucher-histories", printTimedMiddleware, printMethodMiddleware, createVoucherHistory);

router.get("/voucher-histories/:historyId", printTimedMiddleware, printMethodMiddleware, getVoucherHistoryById);

router.put("/voucher-histories/:historyId", printTimedMiddleware, printMethodMiddleware, updateVoucherHistoryById);

router.delete("/voucher-histories/:historyId", printTimedMiddleware, printMethodMiddleware, deleteVoucherHistoryById);

// Export dữ liệu 1 module
module.exports = router;

