const printTimedMiddleware = (request, response, next) => {
    console.log("Time", new Date());
    next();
}

module.exports = {
    printTimedMiddleware: printTimedMiddleware
}
