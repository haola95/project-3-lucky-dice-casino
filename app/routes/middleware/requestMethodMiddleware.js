const printMethodMiddleware = (request, response, next) => {
    console.log("Request Method: ", request.method);
    next();
}

module.exports = {
    printMethodMiddleware: printMethodMiddleware
}


